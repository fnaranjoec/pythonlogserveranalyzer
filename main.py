import array as arr
import datetime
import csv

from operator import itemgetter, attrgetter

from tkinter import *
from tkinter.ttk import *
from tkinter.filedialog import askopenfilename
from tkinter.messagebox import showerror
from tkinter import messagebox as mbox

import os.path

class Application(Frame):

    def __init__(self, master):
        #Inicializo el frame
        Frame.__init__(self, master)
        self.grid()

        #Frame.pack(self, fill=NONE)

        menubar = Menu(master)
        master['menu'] = menubar

        menu_file = Menu(menubar)
        menu_file.add_command(label="Cargar Archivo Log", command=self.cargarLog)
        menu_file.add_command(label="Salir del Programa", command=self.salir)

        menubar.add_cascade(menu=menu_file, label="Archivo")


        #Creo el frame principal
        self.frame = Frame(master)

        #Creo el notebook (tabs)
        self.notebook = Notebook(self.frame)
        #creo las hojas del notebook (2 tab)
        f1 = Frame(self.notebook, width=800, height=600)
        f2 = Frame(self.notebook, width=800, height=600)

        #agrego las hojas al notebook
        self.notebook.add(f1, text="Detalle de Consumos KB/s")
        self.notebook.add(f2, text="Resumen de Consumos")

        #agregar lista encolumnada
        self.lista_detalle(f1)
        self.lista_resumen(f2)


        #creo status bar
        self.statusMensaje = StringVar()
        self.status = Label(self.frame, textvariable=self.statusMensaje, relief=RAISED, anchor=W)
        self.status.grid(row=1, column=0, sticky=(N, S, E, W))
        self.statusMensaje.set('Listo...')

        #layout de notebook
        self.frame.grid(row=0 , column=0, sticky=(N, S, E, W))
        self.notebook.grid(row=0, column=0, sticky=(N, S, E, W))

        #resize
        master.columnconfigure(0, weight=1)
        master.rowconfigure(0, weight=1)
        self.frame.columnconfigure(0, weight=2)
        self.frame.rowconfigure(0, weight=2)




    def lista_detalle(self, parent):
        columns= ('Fecha', 'Hora', 'Estacion', 'Cliente', 'Usuario', 'Conexiones', 'Consumo Bandwith KB/s', 'Tiempo Conexion /s')
        self.ctree_detalle = Treeview(parent, columns=columns, show="headings") #, selectmode="browse"

        scrollbar_horizontal_detalle = Scrollbar(parent, orient='horizontal', command=self.ctree_detalle.xview)
        scrollbar_vertical_detalle = Scrollbar(parent, orient='vertical', command=self.ctree_detalle.yview)
        scrollbar_horizontal_detalle.pack(side='bottom', fill=X)
        scrollbar_vertical_detalle.pack(side='right', fill=Y)
        self.ctree_detalle.configure(xscrollcommand=scrollbar_horizontal_detalle.set,
                                     yscrollcommand=scrollbar_vertical_detalle.set)

        self.ctree_detalle.column(0, width=10, anchor=W)
        self.ctree_detalle.heading(0, text="Fecha", anchor=W)

        self.ctree_detalle.column(1, width=10, anchor=W)
        self.ctree_detalle.heading(1, text="Hora", anchor=W)

        self.ctree_detalle.column(2, width=10, anchor=W)
        self.ctree_detalle.heading(2, text="Estacion", anchor=W)

        self.ctree_detalle.column(3, width=5, anchor=W)
        self.ctree_detalle.heading(3, text="Cliente", anchor=W)

        self.ctree_detalle.column(4, width=5, anchor=W)
        self.ctree_detalle.heading(4, text="Usuario", anchor=W)

        self.ctree_detalle.column(5, width=3, anchor=E)
        self.ctree_detalle.heading(5, text="Conexiones", anchor=E)

        self.ctree_detalle.column(6, width=20, anchor=E)
        self.ctree_detalle.heading(6, text="Consumo Bandwith KB/s", anchor=E)

        self.ctree_detalle.column(7, width=20, anchor=E)
        self.ctree_detalle.heading(7, text="Tiempo Conexion /s", anchor=E)

        self.ctree_detalle.pack(fill=BOTH, expand=1)


    def lista_resumen(self, parent):
        columns= ('Estacion', 'Usuario', 'Conectados', 'Consumo Bandwith KB/s', 'Tiempo Conexion /s')
        self.ctree_resumen = Treeview(parent, columns=columns, show="headings") #, selectmode="browse"

        scrollbar_horizontal_resumen = Scrollbar(parent, orient='horizontal', command=self.ctree_resumen.xview)
        scrollbar_vertical_resumen = Scrollbar(parent, orient='vertical', command=self.ctree_resumen.yview)
        scrollbar_horizontal_resumen.pack(side='bottom', fill=X)
        scrollbar_vertical_resumen.pack(side='right', fill=Y)
        self.ctree_resumen.configure(xscrollcommand=scrollbar_horizontal_resumen.set,
                                     yscrollcommand=scrollbar_vertical_resumen.set)

        self.ctree_resumen.column(0, width=10, anchor=W)
        self.ctree_resumen.heading(0, text="Estacion", anchor=W)

        self.ctree_resumen.column(1, width=5, anchor=W)
        self.ctree_resumen.heading(1, text="Usuario", anchor=W)

        self.ctree_resumen.column(2, width=3, anchor=E)
        self.ctree_resumen.heading(2, text="Conexiones", anchor=E)

        self.ctree_resumen.column(3, width=20, anchor=E)
        self.ctree_resumen.heading(3, text="Consumo Bandwith KB/s", anchor=E)

        self.ctree_resumen.column(4, width=20, anchor=E)
        self.ctree_resumen.heading(4, text="Tiempo Conexion /s", anchor=E)

        self.ctree_resumen.pack(fill=BOTH, expand=1)





    def setStatus(self, message):
        self.status["text"] = message


    def cargarLog(self):
        fname = askopenfilename(filetypes=(("Log files", "*.log"),))
        if fname:
            try:
                self.analizarLog(fname)
                #print("""here it comes: self.settings["template"].set(fname)""")
            except:  # <- naked except is a bad idea
                showerror("Open Source File", "Failed to read file\n'%s'" % fname)
            return


    def salir(self):
        self.quit()


    def generarCSV(self, filecsv, filelog):

        # Cuento numero de lineas
        TotalLines = 0
        f = open(filelog, 'r')

        for TotalLines, l in enumerate(f):
            pass

        TotalLines += 1
        f.close()

        # --------------- Genero Archivo CSV
        # Abro archivo csv para escritura
        f2 = open(filecsv, "w+")
        print('Abri CSV para escritura...')

        # Inicializo variables
        accepted = False
        index1 = 0
        index2 = 0
        index3 = 0
        fecha = ''
        hora = ''
        fechaConexion = ''
        horaConexion = ''
        estacion = ''
        usuarioid = ''
        clienteid = ''
        clientes = 0
        bandwith = 0.000000
        bandwithCliente = 0.000000

        # Abro archivo log para analisis y obtengo todas las lineas
        f = open(filelog, 'r')
        print('Abri LOG para lectura...')
        flines = f.readlines()
        print('Lei todas las lineas ...')

        # Iteracion sobre cada linea del archivo log
        xLine = 0
        print('Procesando linea x linea ...')
        for x in flines:

            # actualio el valor del progressbar
            xLine += 1

            self.statusMensaje.set('Procesando linea : ' + str(xLine) + ' / ' + "{:10,.0f}".format(((xLine / TotalLines) * 100)) + '%')
            self.status.update()

            # Si es el inicio de conexion de cliente
            if 'Accepted client ' in x:
                # En cada nueva conexion reinicio las variables
                accepted = True
                bandwith = 0.000000
                bandwithCliente = 0.000000
                estacion = ''
                clienteid = ''
                usuarioid = ''
                fechaConexion = ''
                horaConexion = ''

                # Obtengo nombre de estacion
                index1 = x.index('[/')
                index2 = x.index(']', index1)
                estacion = x[index1 + 2:index2]

                # Obtengo id de cliente
                index1 = x.index('Accepted client ')
                index2 = x.index('[', index1)
                clienteid = x[index1 + 16:index2]

                # Obtengo id de usuario
                index1 = x.index('Accepted client ')
                index2 = x.index('[', index1)
                index3 = x.index(']', index2)
                usuarioid = x[index2 + 1:index3]

                fechahoraConexion = datetime.datetime.strptime((x[1:12] + " " + x[13:21]), "%d/%b/%Y %H:%M:%S").strftime("%Y-%m-%d %H:%M:%S")


            # Analizo consumo de ancho de banda solo si se acepto conexion
            if ('Calendar Thread' in x) and ('Clients:' in x) and (accepted):

                # Ontengo clientes conectado
                index1 = x.index('Clients:')
                clientes = x[index1 + 8]

                # Obtengo fecha y hora
                fecha = datetime.datetime.strptime(x[1:12], "%d/%b/%Y").strftime("%Y-%m-%d")  # "%d/%m/%Y"
                hora = x[13:21]
                #fechahoraConsumo = datetime.datetime.strptime((x[1:12] + " " + x[13:21]), "%d/%b/%Y %H:%M:%S").strftime("%Y-%m-%d %H:%M:%S")

                # Solo grabo si clientes conectados es mayor a 0 para no analizar trafico propio del servidor
                if (int(clientes) > 0):
                    index1 = x.index('Bandwidth:')
                    index2 = x.index('KB/s', index1)
                    bandwith = float(x[index1 + 10:index2])
                    bandwithCliente += bandwith

                    f2.write('{0},{1},{2},{3},{4},{5},{6},{7}\n'.format(fecha, hora, estacion.rstrip(),
                                                                    clienteid.rstrip(), usuarioid.rstrip(),
                                                                    clientes.rstrip(), bandwith, 0.00))

            # Grabo consumo en cero si la siguiente linea de conexio es un Kicking o desconectado por que no hubo consumo
            if ('Kicking client ' in x) :
                accepted = False

                fecha = datetime.datetime.strptime(x[1:12], "%d/%b/%Y").strftime("%Y-%m-%d")  # "%d/%m/%Y"
                hora = x[13:21]
                fechahoraConsumo = datetime.datetime.strptime((x[1:12] + " " + x[13:21]), "%d/%b/%Y %H:%M:%S").strftime(
                    "%Y-%m-%d %H:%M:%S")

                tiempoConexion = 0
                tiempoConexion = (datetime.datetime.strptime(fechahoraConsumo, "%Y-%m-%d %H:%M:%S") -
                                  datetime.datetime.strptime(fechahoraConexion, "%Y-%m-%d %H:%M:%S")).total_seconds()

                f2.write(
                    '{0},{1},{2},{3},{4},{5},{6},{7}\n'.format(fecha, hora, estacion.rstrip(), clienteid.rstrip(),
                                                               usuarioid.rstrip(), 0, 0.000000, tiempoConexion))
                #if (bandwithCliente)==0.000000:



        # Cierro archivo del log
        f.close()

        # Cierro archivo csv
        f2.close


    def generarReporte(self, filecsv):
        #-------------- SOLO PROCESO DIRECTO CSV
        self.statusMensaje.set('Generando Matriz Espere...')
        self.status.update()

        # Abro archivo csv
        results = []
        with open(filecsv) as csvfile:
            reader = csv.reader(csvfile, quoting=csv.QUOTE_NONE)  # change contents to floats
            for row in reader:  # each row is a list
                results.append(row)

        # Convertir columnas de fecha, hora, clientes y consumoBandwith
        self.statusMensaje.set('Convirtiendo fecha, hora, clientes, consumoBandwith, espere por favor...')
        self.status.update()
        TotalLines = len(results)
        for i in range(TotalLines):
            results[i][5] = int(results[i][5])
            results[i][6] = float(results[i][6])
            results[i][7] = float(results[i][7])

        self.statusMensaje.set('Creando listas, espere por favor...')
        self.status.update()
        resultsTuple = []
        TotalLines = len(results)
        for i in range(TotalLines):
            resultsTuple.append(tuple(results[i]))

        # Ordeno por Fecha y Hora
        resultsFinal = []
        self.statusMensaje.set('Ordenando para detalle, espere por favor...')
        self.status.update()
        resultsFinal = sorted(tuple(map(tuple, resultsTuple)), key=itemgetter(0, 1))


        #-------------------------------------> DETALLE <-----------------------------

        self.statusMensaje.set('Creando detalle, espere por favor...')
        self.status.update()
        TotalLines = len(resultsFinal)
        for i in range(TotalLines):
            # print(resultsFinal[i])
            self.ctree_detalle.insert('', 'end',
                                      values=(resultsFinal[i][0], resultsFinal[i][1], resultsFinal[i][2],
                                              resultsFinal[i][3], resultsFinal[i][4],
                                              "{:10,.0f}".format(resultsFinal[i][5]),
                                              "{:10,.6f}".format(resultsFinal[i][6]),
                                              "{:10,.2f}".format(resultsFinal[i][7])))

        # Ordeno por Estacion, Usuario, Fecha y Hora
        self.statusMensaje.set('Ordenando para resumen, espere por favor...')
        self.status.update()
        #Generando Tuplas
        resultsFinal = sorted(tuple(map(tuple, resultsTuple)), key=itemgetter(2, 4, 0, 1))
        
        #Incializando variables de control
        estacionControl = ''
        usuarioControl = ''
        estacionControl = resultsFinal[0][2]
        usuarioControl = resultsFinal[0][4]

        #Incializando acumuladores por usuario
        conexionesUsuario = 0
        consumoBandwithUsuario = 0.000000
        tiempoConexionUsuario = 0.00

        #Incializando acumuladores por estacion
        conexionesEstacion = 0
        consumoBandwithEstacion = 0.000000
        tiempoConexionEstacion = 0.00

        #Incializando acumuladores del gran total
        conexionesTotal = 0
        consumoBandwithTotal = 0.000000
        tiempoConexionTotal = 0.00

        #-------------------------------------> RESUMEN <-----------------------------

        self.statusMensaje.set('Creando resumen, espere por favor...')
        self.status.update()

        self.ctree_resumen.tag_configure('estacion', foreground='#E8E8E8')
        self.ctree_resumen.tag_configure('total', foreground='#DFDFDF')

        TotalLines = len(resultsFinal)

        for i in range(TotalLines):

            if (resultsFinal[i][2] != estacionControl):

                #Escribo ultimo usuario
                self.ctree_resumen.insert('', 'end',
                                          values=(estacionControl, usuarioControl,
                                                  "{:10,.0f}".format(conexionesUsuario),
                                                  "{:10,.6f}".format(consumoBandwithUsuario),
                                                  "{:10,.2f}".format(tiempoConexionUsuario)),
                                          tag='estacion'
                                          )


                #Actualizo varibles de quiebre de control
                estacionControl = resultsFinal[i][2]
                usuarioControl = resultsFinal[i][4]

                #acumulo ultimo usuario
                conexionesEstacion += conexionesUsuario
                consumoBandwithEstacion += consumoBandwithUsuario
                tiempoConexionEstacion += tiempoConexionUsuario

                conexionesUsuario = 0
                consumoBandwithUsuario = 0.000000
                tiempoConexionUsuario = 0.00

                conexionesUsuario += resultsFinal[i][5]
                consumoBandwithUsuario += resultsFinal[i][6]
                tiempoConexionUsuario += resultsFinal[i][7]

                #escribo ultima estacion
                self.ctree_resumen.insert('', 'end', values=(
                '', '           TOTAL ' + estacionControl,
                "{:10,.0f}".format(conexionesEstacion),
                "{:10,.6f}".format(consumoBandwithEstacion),
                "{:10,.2f}".format(tiempoConexionEstacion)))

                conexionesTotal += conexionesEstacion
                consumoBandwithTotal += consumoBandwithEstacion
                tiempoConexionTotal += tiempoConexionEstacion

                conexionesEstacion = 0
                consumoBandwithEstacion = 0.000000
                tiempoConexionEstacion = 0.00


            else:
                if (resultsFinal[i][4] != usuarioControl):
                    self.ctree_resumen.insert('', 'end',
                                              values=(estacionControl, usuarioControl,
                                                      "{:10,.0f}".format(conexionesUsuario),
                                                      "{:10,.6f}".format(consumoBandwithUsuario),
                                                      "{:10,.2f}".format(tiempoConexionUsuario)))

                    #Actualizo variable de quiebre de control
                    usuarioControl = resultsFinal[i][4]

                    conexionesEstacion += conexionesUsuario
                    consumoBandwithEstacion += consumoBandwithUsuario
                    tiempoConexionEstacion += tiempoConexionUsuario

                    conexionesUsuario = 0
                    consumoBandwithUsuario = 0.000000
                    tiempoConexionUsuario = 0.00

                    conexionesUsuario += resultsFinal[i][5]
                    consumoBandwithUsuario += resultsFinal[i][6]
                    tiempoConexionUsuario += resultsFinal[i][7]
                else:
                    conexionesUsuario += resultsFinal[i][5]
                    consumoBandwithUsuario += resultsFinal[i][6]
                    tiempoConexionUsuario += resultsFinal[i][7]


        #Proceso el final del archivo CSV
        self.ctree_resumen.insert('', 'end',
                                  values=(estacionControl, usuarioControl,
                                          "{:10,.0f}".format(conexionesUsuario),
                                          "{:10,.6f}".format(consumoBandwithUsuario),
                                          "{:10,.2f}".format(tiempoConexionUsuario)))
        # acumulo ultimo usuario

        conexionesEstacion += conexionesUsuario
        consumoBandwithEstacion += consumoBandwithUsuario
        tiempoConexionEstacion += tiempoConexionUsuario

        conexionesUsuario = 0
        consumoBandwithUsuario = 0.000000
        tiempoConexionUsuario = 0.00

        # escribo ultima estacion
        self.ctree_resumen.insert('', 'end', values=(
            '', '           TOTAL ' + estacionControl,
                            "{:10,.0f}".format(conexionesEstacion),
                            "{:10,.6f}".format(consumoBandwithEstacion),
                            "{:10,.2f}".format(tiempoConexionEstacion)))

        self.ctree_resumen.tag_configure('total', background='#DFDFDF')

        conexionesTotal += conexionesEstacion
        consumoBandwithTotal += consumoBandwithEstacion
        tiempoConexionTotal += tiempoConexionEstacion

        conexionesEstacion = 0
        consumoBandwithEstacion = 0.000000
        tiempoConexionEstacion = 0.00

        self.ctree_resumen.insert('', 'end', values=(
            'GRAN TOTAL ', '' ,
                            "{:10,.0f}".format(conexionesTotal),
                            "{:10,.6f}".format(consumoBandwithTotal),
                            "{:10,.2f}".format(tiempoConexionTotal)),
                            tag='total'
        )


    def analizarLog(self, filelog):

        filecsv = filelog[0:len(filelog)-4] + ".csv"

        #verifico si existe o no el archivo
        if (os.path.exists(filecsv)):
            if mbox.askquestion("El log seleccionado ya se analizo.", "Desea volver a analizarlo?", icon='warning') == 'yes':
                os.remove(filecsv)
                self.generarCSV(filecsv, filelog)
        else:
            self.generarCSV(filecsv, filelog)

        self.generarReporte(filecsv)
        self.statusMensaje.set('Termino...')
        self.status.update()


def main():
    #Crear ventana
    root = Tk()

    #Modificar propiedades de la ventana
    root.title("Log Analisis App")
    root.geometry("800x600")

    #creo instancia de la clase Application
    app = Application(root)

    #Lanzar ventana
    root.mainloop()


if __name__ == '__main__':
  main()

